const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

app.post('/currency', (req, res) => {
    if(!req.body.hasOwnProperty('name')){
        return res.status(400).send({
            'error' : 'Bad Request : currency not supported'
        });
    }    
	if(typeof req.body.name !== 'string'){
		return res.status(400).send({
			'error' : 'Bad Request : name not a string' 
		})
	}	
    if (req.body.name === '') {
        return res.status(400).send({
            'error' : 'Bad Request : name is empty'
        });
    }
    if (!req.body.hasOwnProperty('ex')) {
        return res.status(400).send({
            'error' : 'Bad Request: ex is missing'
        });
    }

    if (typeof req.body.ex !== 'object') {
        return res.status(400).send({
            'error' : 'Bad Request: ex must be an object'
        });
    }
    if (req.body.ex === '') {
        return res.status(400).send({
            'error' : 'Bad Request : ex is empty'
        });
    }
    if(!req.body.hasOwnProperty('alias')){
        return res.status(400).send({
            'error' : 'Bad Request : alias is missing'
        });
    }   
    if(typeof req.body.alias !== 'string'){
		return res.status(400).send({
			'error' : 'Bad Request : alias not a string' 
		})
	}	
    if (req.body.alias === '') {
        return res.status(400).send({
            'error' : 'Bad Request : alias is empty'
        });
    }
     if (req.body.hasOwnProperty('alias')) {
        const alias = req.body.alias;
        const existingCurrencies = Object.keys(exchangeRates);
        if (existingCurrencies.includes(alias)) {
            return res.status(400).send({
                'error' : 'Bad Request: Duplicate alias'
            });
        }
    }

   return res.status(200).send({'data': {} });
});

}
